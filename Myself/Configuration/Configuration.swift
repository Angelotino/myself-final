//
//  Configuration.swift
//  Myself
//
//  Created by Angelo on 02.06.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

class Configuration {

    static let cellCornerRadius: CGFloat = 5
    static let highCornerRadius: CGFloat = 40
    static let navigationBarAlpha: CGFloat = 1.0
    static let navigationBarHalfAlpha: CGFloat = 0.36
}
