//
//  Constants.swift
//  Myself
//
//  Created by Michal Severín on 08.08.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

class Constants {

    static let appName = "Myself"
    static let basicUrl = "http://130.193.12.189:3000/"
    static let cdMe = "Me"
    static let name = "Michal Severín"
}
