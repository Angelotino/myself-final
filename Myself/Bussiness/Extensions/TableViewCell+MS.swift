//
//  TableViewCell.swift
//  Myself
//
//  Created by Angelo on 25.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

extension UITableViewCell {

    static var identifier: String {
        return NSStringFromClass(classForCoder())
    }
}
