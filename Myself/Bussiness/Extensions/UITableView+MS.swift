//
//  UITableView+MS.swift
//  Myself
//
//  Created by Angelo on 07.06.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit

protocol ReusableView {
    static var identifier: String { get }
}

extension ReusableView where Self: UIView {
    static var identifier: String {
        return NSStringFromClass(classForCoder())
    }
}

extension UITableViewCell: ReusableView {}

extension UITableView {
    func register(cellClass: UITableViewCell.Type) {
        registerClass(cellClass.self, forCellReuseIdentifier: cellClass.identifier)
    }

    func dequeueReusableCell<T: UITableViewCell where T: ReusableView>(indexPath: NSIndexPath) -> T {
        guard let cell = self.dequeueReusableCellWithIdentifier(T.identifier, forIndexPath: indexPath) as? T else {
            fatalError("Enable to dequeue a cell with identifier. \(T.identifier)")
        }
        return cell
    }

    func reloadSection<T: RawRepresentable where T.RawValue == Int>(index index: T, animation: UITableViewRowAnimation = .Fade) {
        beginUpdates()
        reloadSections(NSIndexSet(index: index.rawValue), withRowAnimation: animation)
        endUpdates()
    }
}
