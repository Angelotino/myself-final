//
//  NavigationBar+MS.swift
//  Myself
//
//  Created by Angelo on 29.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

extension UINavigationBar {

    func backgroundImageWithAlpha(color: UIColor, alpha: CGFloat) {
        setBackgroundImage(UIColor.imageWithColor(color.colorWithAlphaComponent(alpha)), forBarMetrics: .Default)
    }
}
