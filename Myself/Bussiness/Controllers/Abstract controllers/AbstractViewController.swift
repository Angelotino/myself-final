//
//  AbstractViewController.swift
//  Myself
//
//  Created by Angelo on 18.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit

class AbstractViewController: UIViewController, InitializeProtocol {

    //MARK: - Lifecycle
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        setupData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        initializeElementsAndApplyConstraints()
    }

    //MARK: <Initialize protocol>
    func initializeElements() {}

    func customSetup() {}

    func endEditing() {
        view.endEditing(true)
    }

    func setupConstraints() {}

    func setupData() {}

    func setupViews() {
        view.backgroundColor = Palette.White.color
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
    }
}
