//
//  AboutMeViewController.swift
//  Myself
//
//  Created by Angelo on 25.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit

class AboutMeViewController: AbstractViewController {

    //MARK: - Properties
    //MARK: -- private properties
    private let ageLabel = Label()
    private let cityLabel = Label()
    private let informationLabel = Label()
    private let emailLabel = Label()
    private let nameLabel = Label()
    private let phoneNumberLabel = Label()

    //MARK: -- public properties
    var infoAboutMe: Me? {
        didSet {
            fetchData()
        }
    }

    //MARK: - Methods
    //MARK: -- initialize method
    internal override func initializeElements() {
        super.initializeElements()

        ageLabel.textColor = Palette.Gray.color

        cityLabel.textColor = Palette.Gray.color

        emailLabel.textColor = Palette.Gray.color

        informationLabel.textColor = Palette.CLightRed.color

        nameLabel.textColor = Palette.Red.color
        nameLabel.font = UIFont.systemFontOfSize(15)

        phoneNumberLabel.textColor = Palette.Gray.color

        for label in [ageLabel, cityLabel, informationLabel, emailLabel, phoneNumberLabel] {
            label.font = UIFont.systemFontOfSize(13)
        }

        for element in [ageLabel, cityLabel, informationLabel, emailLabel, phoneNumberLabel, nameLabel] {
            view.addSubview(element)
        }
    }

    //MARK: -- setup methods
    internal override func setupData() {
        super.setupData()

        do {
            try Me.fetchData()
            if let object = Me.object {
                infoAboutMe = object
            }
            else {
                InfoLogger.defaultLogger.error("No data")
                InfoAboutMeRequest().execute { result in
                    switch result {
                    case let .Success(model):
                        do {
                            if let user = model {
                                try Me.newFromObject(user)
                            }
                        }
                        catch {
                            CoreDataErrors.EnableInsertRequest
                        }
                        InfoLogger.defaultLogger.verbose("User data loaded")
                    case .Failure:
                        InfoLogger.defaultLogger.error("No internet connection")
                    }
                }
            }
        }
        catch {
            InfoLogger.defaultLogger.error("Enable fetch data")
        }
    }

    private func fetchData() {
        if let infoAboutMe = infoAboutMe {
            ageLabel.text = "\(StringProvider.Prime.Age.localizedString): \(infoAboutMe.mAge ?? 0)"
            cityLabel.text = "\(StringProvider.Prime.City.localizedString): \(infoAboutMe.mCity ?? String.empty)"
            informationLabel.text = "\(StringProvider.Prime.Information.localizedString): \(infoAboutMe.mDescription ?? String.empty)"
            emailLabel.text = "\(StringProvider.Prime.Email.localizedString): \(infoAboutMe.mEmail ?? String.empty)"
            nameLabel.text = "\(StringProvider.Prime.Name.localizedString): \(infoAboutMe.mName ?? String.empty)"
            phoneNumberLabel.text = "\(StringProvider.Prime.PhoneNumber.localizedString): \(infoAboutMe.mPhoneNumber ?? 0)"
        }
    }

    internal override func setupViews() {
        super.setupViews()

        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(icon: .MenuButton), style: .Plain, target: self, action: #selector(openLeftMenuButtonPressed(_:)))
    }

    func openLeftMenuButtonPressed(sender: AnyObject) {
        self.mm_drawerController.openDrawerSide(.Left, animated: true, completion: nil)
    }

    //MARK: - Constraints
    internal override func setupConstraints() {
        super.setupConstraints()

        ageLabel.snp_makeConstraints { make in
            make.leading.equalTo(view).inset(10)
            make.trailing.equalTo(view).inset(10)
            make.top.equalTo(emailLabel.snp_bottom).offset(10)
        }

        cityLabel.snp_makeConstraints { make in
            make.leading.equalTo(view).inset(10)
            make.trailing.equalTo(view).inset(10)
            make.top.equalTo(ageLabel.snp_bottom).offset(10)
        }

        informationLabel.snp_makeConstraints { make in
            make.leading.equalTo(view).inset(10)
            make.trailing.equalTo(view).inset(10)
            make.top.equalTo(phoneNumberLabel.snp_bottom).offset(15)
        }

        emailLabel.snp_makeConstraints { make in
            make.leading.equalTo(view).inset(10)
            make.trailing.equalTo(view).inset(10)
            make.top.equalTo(nameLabel.snp_bottom).offset(10)
        }

        nameLabel.snp_makeConstraints { make in
            make.leading.equalTo(view).inset(10)
            make.trailing.equalTo(view).inset(10)
            make.top.equalTo(view).offset(80)
        }

        phoneNumberLabel.snp_makeConstraints { make in
            make.leading.equalTo(view).inset(10)
            make.trailing.equalTo(view).inset(10)
            make.top.equalTo(cityLabel.snp_bottom).offset(10)
        }
    }
}
