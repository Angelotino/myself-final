//
//  PrimeViewController.swift
//  Myself
//
//  Created by Angelo on 10.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit
import SnapKit

class PrimeViewController: AbstractViewController {

    //MARK: - Properties
    //MARK: -- private properties
    private let myselfImageView =  ImageView()

    private let developerLabel = Label()
    private let nameLabel = Label()
    private let descriptionLabel = Label()

    //MARK: - Methods
    //MARK: -- initialize method
    internal override func initializeElements() {
        super.initializeElements()

        descriptionLabel.text = StringProvider.Prime.Description.localizedString
        descriptionLabel.textColor = Palette.CLightBlue.color
        descriptionLabel.font = UIFont.systemFontOfSize(14)

        developerLabel.text = StringProvider.Prime.Developer.localizedString
        developerLabel.font = UIFont.systemFontOfSize(13)

        myselfImageView.image = UIImage(image: .Me)
        myselfImageView.layer.cornerRadius = Configuration.highCornerRadius
        myselfImageView.contentMode = .ScaleAspectFit
        myselfImageView.layer.borderWidth = 0.8
        myselfImageView.layer.borderColor = Palette.CLightBlue.cgColor
        myselfImageView.clipsToBounds = true

        nameLabel.text = Constants.name

        for element in [developerLabel, myselfImageView, nameLabel, descriptionLabel] {
            view.addSubview(element)
        }
    }

    //MARK: -- setup methods
    internal override func setupData() {
        super.setupData()

        InfoAboutMeRequest().execute { result in
            switch result {
            case let .Success(model):
                do {
                    if let user = model {
                        try Me.newFromObject(user)
                    }
                }
                catch {
                    CoreDataErrors.EnableInsertRequest
                }
                InfoLogger.defaultLogger.verbose("User data loaded")
            case .Failure:
                InfoLogger.defaultLogger.error("No internet connection")
            }
        }
    }

    internal override func setupViews() {
        super.setupViews()

        title = StringProvider.Prime.Programming.localizedString
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(icon: .MenuButton), style: .Plain, target: self, action: #selector(openLeftMenuButtonPressed))
    }

    //MARK: -- action methods
    func openLeftMenuButtonPressed() {
        self.mm_drawerController.openDrawerSide(.Left, animated: true, completion: nil)
    }

    //MARK: - Constraints
    internal override func setupConstraints() {
        super.setupConstraints()

        descriptionLabel.snp_makeConstraints { make in
            make.leading.equalTo(view).inset(10)
            make.trailing.equalTo(view).inset(10)
            make.top.equalTo(myselfImageView.snp_bottom).offset(10)
        }

        developerLabel.snp_makeConstraints { make in
            make.leading.equalTo(view).inset(10)
            make.top.equalTo(nameLabel.snp_top).inset(20)
            make.trailing.equalTo(myselfImageView.snp_trailing).offset(10)
        }

        myselfImageView.snp_makeConstraints { make in
            make.trailing.equalTo(view).inset(10)
            make.top.equalTo(view.snp_top).inset(75)
            make.width.equalTo(80)
            make.height.equalTo(80)
        }

        nameLabel.snp_makeConstraints { make in
            make.leading.equalTo(view).inset(10)
            make.top.equalTo(view.snp_top).inset(75)
            make.trailing.equalTo(myselfImageView.snp_trailing).offset(10)
        }
    }
}
