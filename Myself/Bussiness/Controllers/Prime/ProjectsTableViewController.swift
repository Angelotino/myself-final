//
//  ProjectsViewController.swift
//  Myself
//
//  Created by Angelo on 25.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit
import Kingfisher

enum Sections: Int {
    case Projects = 0
}

class ProjectsTableViewController: AbstractTableViewController {

    //MARK: - Properties
    //MARK: -- private properties
    private var projects: ProjectList? {
        didSet {
            tableView.reloadSection(index: Sections.Projects)
        }
    }

    //MARK: - Methods
    //MARK: -- setup methods
    internal override func setupData() {
        super.setupData()

        reloadData()
    }

    internal override func setupViews() {
        super.setupViews()

        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0)
        tableView.register(ProjectTableViewCell.self)

        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(icon: .MenuButton), style: .Plain, target: self, action: #selector(openLeftMenuButtonPressed(_:)))
    }

    //MARK: -- action methods
    private func reloadData() {

        ProjectListRequest().execute { result in
            switch result {
            case let .Success(projects):
                self.projects = projects
            case .Failure:
                InfoLogger.defaultLogger.error("Data error")
            }
        }
    }

    func openLeftMenuButtonPressed(sender: AnyObject) {
        self.mm_drawerController.openDrawerSide(.Left, animated: true, completion: nil)
    }

    //MARK: - Constraints
    internal override func setupConstraints() {
        super.setupConstraints()
    }

    //MARK: <UITableViewDelegate>
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath) as ProjectTableViewCell

        if indexPath.row < projects?.projects.count {
            if let project = projects?.projects.sort({ $0.name < $1.name })[indexPath.row] {
                cell.detail = project.language
                cell.imageURLString = project.image
                cell.title = project.name
            }
        }
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.row < projects?.projects.count {
            let projectDetail = ProjectDetailViewController()
            projectDetail.projectDetail = projects?.projects.sort({ $0.name < $1.name })[indexPath.row]
            mm_drawerController.openDrawerGestureModeMask =  MMOpenDrawerGestureMode.None
            navigationController?.navigationBar.backgroundImageWithAlpha(Palette.Black.color, alpha: Configuration.navigationBarHalfAlpha)
            navigationController?.pushViewController(projectDetail, animated: true)
        }
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects?.projects.count ?? 0
    }
}
