//
//  RootViewController.swift
//  Myself
//
//  Created by Angelo on 11.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

class RootViewController {

    static var rootViewController: MMDrawerController {
        let leftMenu = LeftViewController()
        let dashBoard = NavigationViewController(rootViewController: PrimeViewController())
        let toReturn = MMDrawerController(centerViewController: dashBoard, leftDrawerViewController: leftMenu)
        toReturn.maximumLeftDrawerWidth = UIScreen.mainScreen().bounds.size.width - 40
        toReturn.showsShadow = true
        toReturn.openDrawerGestureModeMask =  MMOpenDrawerGestureMode.All
        toReturn.closeDrawerGestureModeMask =  MMCloseDrawerGestureMode.All
        return toReturn
    }    
}
