//
//  Execute.swift
//  Myself
//
//  Created by Angelo on 22.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Alamofire
import Alamofire_SwiftyJSON
import Foundation
import SwiftyJSON

protocol JSONObject {
    init?(json: JSON)
}

enum Result<T: JSONObject> {
    case Failure
    case Success(T?)
}

protocol ExecuteProtocol: URLManagerProtocol {
    associatedtype T: JSONObject
    func execute(completionHandler: (result: Result<T>) -> Swift.Void)
}

extension ExecuteProtocol {
    func execute(completionHandler: (result: Result<T>) -> Swift.Void) {
        Alamofire.request(method, url, parameters: parameters).responseSwiftyJSON(options: .AllowFragments) {
            request, response, json, error in
            if error == nil {
                if let statusCode = response?.statusCode {
                    switch ApiErrorChecker(statusCode: statusCode) {
                    case .Success:
                        completionHandler(result: .Success(T(json: json)))
                    case let .Error(error):
                        print(error)
                        completionHandler(result: .Failure)
                    }
                }
            }
        }
    }
}
