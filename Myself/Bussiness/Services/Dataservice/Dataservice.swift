//
//  Dataservice.swift
//  Myself
//
//  Created by Angelo on 22.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation
import UIKit

final class Dataservice {

    static let sharedInstance = Dataservice()
    lazy var context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    private init() {}
}
