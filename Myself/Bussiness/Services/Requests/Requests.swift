//
//  Requests.swift
//  Myself
//
//  Created by Michal Severín on 08.08.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

enum Requests {
    case InfoAboutMeRequest
    case ProjectsRequest
}
