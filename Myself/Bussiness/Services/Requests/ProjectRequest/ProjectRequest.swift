//
//  ProjectRequest.swift
//  Myself
//
//  Created by Michal Severín on 08.08.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

struct ProjectListRequest: ExecuteProtocol {
    typealias T = ProjectList

    var request: Requests {
        return .ProjectsRequest
    }
}
