//
//  InfoAboutMeRequest.swift
//  Myself
//
//  Created by Michal Severín on 08.08.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

struct InfoAboutMeRequest: ExecuteProtocol  {
    typealias T = AboutMeList

    var request: Requests {
        return .InfoAboutMeRequest
    }
}
