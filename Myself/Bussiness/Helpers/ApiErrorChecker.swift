//
//  ApiErrorChecker.swift
//  Myself
//
//  Created by Angelo on 22.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

enum ApiErrorChecker {
    case Success
    case Error(String)

    init(statusCode: Int) {
        switch statusCode {
        case 200..<210:
            self = .Success
        default:
            self = .Error("Error")
        }
    }
}
