//
//  URLManager.swift
//  Myself
//
//  Created by Angelo on 24.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation
import Alamofire

protocol URLManagerProtocol {
    var parameters: [String: AnyObject] { get }
    var url: String { get }
    var method: Alamofire.Method { get }
    var request: Requests { get }
}

extension URLManagerProtocol {
    var parameters: [String: AnyObject] {
        switch request {
        case .InfoAboutMeRequest:
            fallthrough
        case .ProjectsRequest:
            return [:]
        }
    }

    var url: String {
        var toReturn = String.empty
        switch request {
        case .InfoAboutMeRequest:
            toReturn = "mes.json"
        case .ProjectsRequest:
            toReturn = "projs.json"
        }
        return Constants.basicUrl+toReturn
    }

    var method: Alamofire.Method {
        switch request {
        case .InfoAboutMeRequest:
            fallthrough
        case .ProjectsRequest:
            return Alamofire.Method.GET
        }
    }
}
