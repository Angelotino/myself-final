//
//  Me+CoreDataProperties.swift
//  Myself
//
//  Created by Angelo on 29.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Me {

    @NSManaged var mAge: NSNumber?
    @NSManaged var mName: String?
    @NSManaged var mPhoneNumber: NSNumber?
    @NSManaged var mEmail: String?
    @NSManaged var mCity: String?
    @NSManaged var mDescription: String?
    @NSManaged var mId: NSNumber?
}
