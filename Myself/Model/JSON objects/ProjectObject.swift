//
//  ProjectObject.swift
//  Myself
//
//  Created by Angelo on 22.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ProjectObject: JSONObject {

    var description: String
    var language: String
    var image: String
    var name: String

    init?(json: JSON) {
        description = json["description"].stringValue
        language = json["language"].stringValue
        image = json["image"].stringValue
        name = json["name"].stringValue
    }
}
