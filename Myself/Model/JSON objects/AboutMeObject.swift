//
//  AboutMeObject.swift
//  Myself
//
//  Created by Angelo on 29.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation
import SwiftyJSON

struct AboutMeObject: JSONObject {

    var age: Int
    var city: String
    var description: String
    var email: String
    var id: Int
    var name: String
    var phoneNumber: Int

    init?(json: JSON) {
        age = json["age"].intValue
        city = json["city"].stringValue
        description = json["description"].stringValue
        email = json["email"].stringValue
        id = json["id"].intValue
        name = json["name"].stringValue
        phoneNumber = json["phonenumber"].intValue
    }
}
