//
//  PalleteColors.swift
//  Myself
//
//  Created by Angelo on 10.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation
import UIKit

enum Palette {
    case Black
    case CLightBlue
    case CLightGreen
    case CLightRed
    case Gray
    case Red
    case White

    var color: UIColor {
        switch self {
        case .Black:
            return UIColor.blackColor()
        case .CLightBlue:
            return UIColor(rgba: 0x42a3d0)
        case .CLightGreen:
            return UIColor(rgba: 0x91dc4f)
        case .CLightRed:
            return UIColor(rgba: 0x330033)
        case .Gray:
            return UIColor.grayColor()
        case .Red:
            return UIColor.redColor()
        case .White:
            return UIColor.whiteColor()
        }
    }

    var cgColor: CGColor {
        return color.CGColor
    }
}
