//
//  StringProvider.swift
//  Myself
//
//  Created by Angelo on 10.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

protocol StringProviderProtocol {
    var localizedString: String { get }
}

enum StringProvider {

    enum Prime: StringProviderProtocol  {
        case AboutMe
        case Age
        case City
        case CoreData
        case Description
        case Developer
        case Email
        case Information
        case Name
        case PhoneNumber
        case Projects
        case Programming

        var localizedString: String {
            var toReturn = String.empty
            switch self {
            case .Age:
                toReturn = "age"
            case .AboutMe:
                toReturn = "aboutMe"
            case .City:
                toReturn = "city"
            case .CoreData:
                toReturn = "coreData"
            case .Description:
                toReturn = "description"
            case .Developer:
                toReturn = "developer"
            case .Email:
                toReturn = "email"
            case .Information:
                toReturn = "information"
            case .Name:
                toReturn = "name"
            case .PhoneNumber:
                toReturn = "phoneNumber"
            case .Projects:
                toReturn = "projects"
            case .Programming:
                toReturn = "programming"
            }
            return toReturn.localizedFromTable(.Prime)
        }
    }
}
