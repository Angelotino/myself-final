//
//  Image.swift
//  Myself
//
//  Created by Angelo on 20.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

extension UIImage {

    enum Asset {

        enum Icon: String {
            case MenuButton = "icon_menuButton"
        }

        enum Image: String {
            case AboutMe = "image_aboutMe"
            case Apple = "image_apple"
            case AppleBg = "image_apple_bg"
            case Me = "image_me"
            case Projects = "image_projects"
        }
    }

    convenience init?(icon: Asset.Icon) {
        self.init(named: icon.rawValue)
    }

    convenience init?(image: Asset.Image) {
        self.init(named: image.rawValue)
    }

    class var emptyImage: UIImage {
        return UIImage()
    }
}
